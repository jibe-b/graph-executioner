GRAPH_run="g:run"
GRAPH_components="<http://jibe-b.github.io/graph/componentsLibrary>"
GRAPH_data="g:clusterDB"

data_directory=${GLOBAL_data_directory}

function cbnqj { curl http://localhost:9999/blazegraph/sparql -H "Content-type:text/x-nquads"                     -H "Accept:application/json" -d  "$1"; }

function launch {

echo "BLZG_LOG=~/.graph-executioner/
/usr/local/blazegraph/bin/blazegraph.sh start 2>> $data_directory/log-blazegraph& 

alternatively:
java -jar /opt/blazegraph/blazegraph-2.1.0.jar

"

}

function change_graph_id {
echo "enter the id of a graph (without <http:// nor>)"
echo "http://"
read graph_id
GRAPH="<http://$graph_id>"
}

function update_executioner() {
# first empty the graph
echo  'clear graph '$GRAPH_components  > $data_directory/clear-graph.sparql
cb u j $data_directory/clear-graph.sparql v 2>> $data_directory/log

#cb u j $data_directory/remove-old-components-from-graph-CHIRURGIE.sparql


# first add the components without the executioner component

GRAPH_components="$GRAPH_components" envsubst < $data_directory/components-graph.trig > $data_directory/components-graph-specifically.trig

sed -i -e '{:a;N;$!ba;s_\n_ _g}' $data_directory/components-graph-specifically.trig

cb l j  $data_directory/components-graph-specifically.trig 2>> $data_directory/log


# then add the graph about executioner component

echo "- load local component graph to database
- get version of the executioner component"

sed 's_\.\ *$_'$GRAPH_components' ._' < $data_directory/graph.nt > $data_directory/graph.nq



cbnqj "$(cat $data_directory/graph.nq)" 2>> $data_directory/log
gitld version

stamp=99999999999999999999999
component=ExecutionerComponent 

echo 'SELECT ?sparql1 WHERE {graph '${GRAPH_components}' {r:ExecutionerComponent sof:sparql_1 ?sp1 . ?sp1 sof:text ?sparql1 .}} ' > $data_directory/ExecutionerComponent-init-sparql.sparql

cb r j $data_directory/ExecutionerComponent-init-sparql.sparql | jq '.results.bindings[0].sparql1.value' | sed -e 's_\^"__;s_\"$__;s_'\''\${GRAPH\_components}'\''_'$GRAPH_components'_g' -f $data_directory/clean-code.sed > $data_directory/select-components-to-start-with.sparql 2>> $data_directory/log

cb r j $data_directory/select-components-to-start-with.sparql > $data_directory/$component$stamp.json 2>> $data_directory/log

echo 'SELECT ?code {
GRAPH '${GRAPH_components}'  {
r:ExecutionerComponent sof:code [ sof:text ?code ] .
}} '> $data_directory/tmp/select-query-for-exe.sparql


cb r j $data_directory/tmp/select-query-for-exe.sparql | jq '.results.bindings[0].code.value' | sed 's_^\"__g; s_\"$__g; s_\\n\ *do\ *\\n_ \; do  _g; s_\\n_ \; _g; s_\\\"_"_g; s_\\\\_\\_g' > $data_directory/$component$stamp.code 2>> $data_directory/log
 
}



function start_executioner() {
stamp=99999999999999999999999

component=ExecutionerComponent

. $data_directory/$component$stamp.code
echo $$ > $data_directory/PID


}


function stop_executioner() {
GRAPH_components="$GRAPH_components" envsubst < $data_directory/STOP.sparql > $data_directory/tmp/STOP.sparql

for i in {0..6}
do
	cb u j $data_directory/tmp/STOP.sparql
	sleep 2
done
}

function force_stop() {
GRAPH_components="$GRAPH_components" envsubst < $data_directory/FORCE-STOP.sparql > $data_directory/tmp/FORCE-STOP.sparql

cb u j $data_directory/tmp/FORCE-STOP.sparql

}




function pause {
echo "FROZEN" > /sys/fs/cgroup/freezer/GraphExecutioner/freezer.state 

}


function resume {
echo "THAWED" > /sys/fs/cgroup/freezer/GraphExecutioner/freezer.state 

}



function clean_folder() {
rm $data_directory/*.code $data_directory/*.json
rm $data_directory/*.res
rm $data_directory/PID
}


case "$1" in
launch )        launch
;;
graph_id )       change_graph_id
;;
update )          update_executioner
;;
start )         start_executioner 
;;
stop )          stop_executioner
;;
force_stop )    force_stop
;;
pause )          pause
;;
resume )          resume
;;

clean )	         clean_folder
;;
* )              echo "help: update, start, stop, clean"


esac


