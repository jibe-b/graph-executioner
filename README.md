## Status

Build status: Passing

Version 0.1.1-beta

Plateform: Linux (tested on Ubuntu 14.04)

## Caution notice

No Waranty provided. Program delivered "as is". Use at your own risks.

## Dependencies

dependencies are installed automatically, except for blazegraph (as it is no yet available in common package repositories)

- blazegraph bigdata nano server (running) (download it at http://www.blazegraph.com/ ). This is a triplestore supporting SPARQL 1.1
- jq [download](https://stedolan.github.io/jq/) - a bash library for json
- curl

# Recommanded development software

- Isaviz 


## How to use this graph executioner

- install it:

> make

> make install

- update the database with the graph

> exe update

Start the executioner

> exe start

Pause the executioner

> exe pause

Resume the executioner

> exe resume

Stop the executioner: /!\ Fails at doing its job. Kill processes named "ExecutionerComponent..." using your task manager

> exe stop

After the executioner has been stopped, you may clean the folder with the !! dangerous !! following command:

> exe clean

## Look at the files produced to know if the executioner works well

Debugging: if a component does not run well, consider giving more time to the component to run inside

r:ExecutionerComponent (see 'sleep 2' in the code)


## To understand the program

- install isaviz <http://www.w3.org/2001/11/IsaViz/>
- import (from file - N-triples) graph.nt

and look at the code of ExecutionerComponent. This piece of code is run at each step unless the code is modified.
