
install-in-this-environment.sh : configuration install.sh
	cat configuration install.sh > install-in-this-environment.sh


.PHONY: install
install :
	bash install-in-this-environment.sh


.PHONY: uninstall
uninstall :
	cat configuration uninstall.sh > uninstall-in-this-environment.sh
	bash uninstall-in-this-environment.sh

.PHONY: clean
clean :
	rm -rf tmp/ install-in-this-environment.sh uninstall-in-this-environment.sh
