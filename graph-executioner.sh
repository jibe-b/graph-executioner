. functions
GRAPH="<http://test-components-20151110>"



function update_executioner() {

sed 's_\.\ *$_<http://test-components-20151110> ._' < graph-test.nt > graph-test.nq ; sed 's_\.\ *$_<http://test-components-20151110> ._' < cax-eq1.nt > cax-eq1.nq  ;cbxu 'clear graph <http://test-components-20151110> ' ; cbnqj "$(cat graph-test.nq cax-eq1.nq )";  stamp=0;component=ExecutionerComponent ; cbj 'prefix r: <http://jibe-b.github.io/resource/>
prefix sof: <http://jibe-b.github.io/ontology/sof/>
SELECT distinct ?component ?sparql1 ?sparql_update1 ?code ?sparql2 ?uri ?sparql_update2 {
graph '${GRAPH}'  {

?uri sof:executionerState "flag" .

?component sof:executionerState "active" ;
                     sof:consumes ?uri ;
                     sof:sparql1 ?sparql1 ;
                     sof:code ?code .

?component sof:sparql2 ?sparql2 ;
                     sof:sparql_update1 ?sparql_update1 ;
                     sof:sparql_update2 ?sparql_update2 .
}

FILTER NOT EXISTS {
graph '${GRAPH}'  {
?uri_2 sof:executionerState "flag" .
?component sof:executionerState "active" ;
                     sof:consumes ?uri2 .
FILTER(?uri_2 != ?uri)
}}}' > $component$stamp.json

cbj 'prefix r: <http://jibe-b.github.io/resource/>
prefix sof: <http://jibe-b.github.io/ontology/sof/>
SELECT ?code {
GRAPH '${GRAPH}'  {
r:ExecutionerComponent sof:code ?code .
}} ' | jq '.results.bindings[0].code.value' | sed 's_^\"__g; s_\"$__g; s_\\n\ *do\ *\\n_ \; do  _g; s_\\n_ \; _g; s_\\\"_"_g; s_\\\\_\\_g' > $component$stamp.code

}



function start_executioner() {
. $component$stamp.code
}

alias s="cbxu '@STOP.sparql' ;cbxu '@STOP.sparql';cbxu '@STOP.sparql';cbxu '@STOP.sparql'"

function stop_executioner() {
s
}

function clean_folder() {
rm ExecutionerComponent*.code ExecutionerComponent*.json
rm TestComponent*
}


case "$1" in
update )          update_executioner
;;
start )         start_executioner 
;;
stop )          stop_executioner
;;
clean )	         clean_folder
;;
* )              echo "help: update, start, stop, clean"


esac


