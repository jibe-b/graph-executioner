OWL 2 RL in SPARQL
==================

Can be imported into any RDF/OWL model to activate all OWL RL rules at
owl:Thing.

Contact: [Holger Knublauch](mailto:holger@topquadrant.com)

This documentation has been automatically generated using
[SWP](http://uispin.org) with
[TopBraid](http://www.topquadrant.com/products/) from the
[SPIN](http://spinrdf.org) profile at
<http://topbraid.org/spin/owlrl-all>.

* * * * *

**Constraints Index: **[cax-adc](#cax-adc) [cax-dw](#cax-dw)
[cls-com](#cls-com) [cls-maxc1](#cls-maxc1) [cls-maxqc1](#cls-maxqc1)
[cls-maxqc2](#cls-maxqc2) [eq-diff1](#eq-diff1) [eq-diff2](#eq-diff2)
[eq-diff3](#eq-diff3) [prp-adp](#prp-adp) [prp-asyp](#prp-asyp)
[prp-irp](#prp-irp) [prp-npa1](#prp-npa1) [prp-npa2](#prp-npa2)
[prp-pdw](#prp-pdw)

**Rules Index: **[cax-eqc1](#cax-eqc1) [cax-eqc2](#cax-eqc2)
[cax-sco](#cax-sco) [cls-avf](#cls-avf) [cls-hv1](#cls-hv1)
[cls-hv2](#cls-hv2) [cls-int1](#cls-int1) [cls-int2](#cls-int2)
[cls-maxc2](#cls-maxc2) [cls-maxqc3](#cls-maxqc3)
[cls-maxqc4](#cls-maxqc4) [cls-oo](#cls-oo) [cls-svf1](#cls-svf1)
[cls-svf2](#cls-svf2) [cls-uni](#cls-uni) [eq-rep-o](#eq-rep-o)
[eq-rep-p](#eq-rep-p) [eq-rep-s](#eq-rep-s) [eq-sym](#eq-sym)
[eq-trans](#eq-trans) [prp-dom](#prp-dom) [prp-eqp1](#prp-eqp1)
[prp-eqp2](#prp-eqp2) [prp-fp](#prp-fp) [prp-ifp](#prp-ifp)
[prp-inv1](#prp-inv1) [prp-inv2](#prp-inv2) [prp-key](#prp-key)
[prp-rng](#prp-rng) [prp-spo1](#prp-spo1) [prp-spo2](#prp-spo2)
[prp-symp](#prp-symp) [prp-trp](#prp-trp) [scm-avf1](#scm-avf1)
[scm-avf2](#scm-avf2) [scm-cls](#scm-cls) [scm-dom1](#scm-dom1)
[scm-dom2](#scm-dom2) [scm-dp](#scm-dp) [scm-eqc1](#scm-eqc1)
[scm-eqc2](#scm-eqc2) [scm-eqp1](#scm-eqp1) [scm-eqp2](#scm-eqp2)
[scm-hv](#scm-hv) [scm-int](#scm-int) [scm-op](#scm-op)
[scm-rng1](#scm-rng1) [scm-rng2](#scm-rng2) [scm-sco](#scm-sco)
[scm-spo](#scm-spo) [scm-svf1](#scm-svf1) [scm-svf2](#scm-svf2)
[scm-uni](#scm-uni)

Constraints
-----------

### cax-adc

``` {.query}
# cax-adc
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 rdfs:label "Shared instance of classes from an AllDisjointClasses block" .
}
WHERE {
    ?y a owl:AllDisjointClasses .
    ?y owl:members ?members .
    ?members list:member ?c1 .
    ?members list:member ?c2 .
    FILTER (?c1 != ?c2) .
    ?x a ?c1 .
    ?x a ?c2 .
}
```

### cax-dw

``` {.query}
# cax-dw
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 rdfs:label "Shared instance of disjoint classes" .
}
WHERE {
    ?c1 owl:disjointWith ?c2 .
    ?x a ?c1 .
    ?x a ?c2 .
}
```

### cls-com

``` {.query}
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 rdfs:label "Resource is instance of two classes that have been declared owl:complementOf" .
}
WHERE {
    ?c1 owl:complementOf ?c2 .
    ?x a ?c1 .
    ?x a ?c2 .
}
```

### cls-maxc1

``` {.query}
# cls-maxc1
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "owl:maxCardinality of 0" .
}
WHERE {
    ?x owl:maxCardinality "0"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?u a ?x .
    ?u ?p ?y .
}
```

### cls-maxqc1

``` {.query}
# cls-maxqc1
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?u .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Maximum qualified cardinality of 0" .
}
WHERE {
    ?x owl:maxQualifiedCardinality "0"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?x owl:onClass ?c .
    ?u a ?x .
    ?u ?p ?y .
    ?y a ?c .
}
```

### cls-maxqc2

``` {.query}
# cls-maxqc2
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?u .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Qualified max cardinality 0" .
}
WHERE {
    ?x owl:maxQualifiedCardinality "0"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?x owl:onClass owl:Thing .
    ?u a ?x .
    ?u ?p ?y .
}
```

### eq-diff1

``` {.query}
# eq-diff1
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath owl:sameAs .
    _:b0 rdfs:label "Violates owl:differentFrom" .
}
WHERE {
    ?x owl:sameAs ?y .
    ?x owl:differentFrom ?y .
}
```

### eq-diff2

``` {.query}
# eq-diff2
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?y1 .
    _:b0 rdfs:label "Violation of owl:AllDifferent" .
}
WHERE {
    ?x a owl:AllDifferent .
    ?x owl:members ?list .
    ?list list:member ?y1 .
    ?list list:member ?y2 .
    FILTER (?y1 != ?y2) .
    ?y1 owl:sameAs ?y2 .
}
```

### eq-diff3

``` {.query}
# eq-diff3
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?y1 .
    _:b0 rdfs:label "Violation of owl:AllDifferent" .
}
WHERE {
    ?x a owl:AllDifferent .
    ?x owl:distinctMembers ?list .
    ?list list:member ?y1 .
    ?list list:member ?y2 .
    FILTER (?y1 != ?y2) .
    ?y1 owl:sameAs ?y2 .
}
```

### prp-adp

``` {.query}
# prp-adp
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath ?p1 .
    _:b0 rdfs:label "Violation of owl:AllDisjointProperties" .
}
WHERE {
    ?z a owl:AllDisjointProperties .
    ?z owl:members ?members .
    ?members list:member ?p1 .
    ?x ?p1 ?y .
    FILTER (?p1 != ?p2) .
    ?x ?p2 ?y .
    ?members list:member ?p2 .
}
```

### prp-asyp

``` {.query}
# prp-asyp
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Asymmetric property" .
}
WHERE {
    ?p a owl:AsymmetricProperty .
    ?x ?p ?y .
    ?y ?p ?x .
}
```

### prp-irp

``` {.query}
# prp-irp
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Irreflexive property" .
}
WHERE {
    ?p a owl:IrreflexiveProperty .
    ?x ?p ?x .
}
```

### prp-npa1

``` {.query}
# prp-npa1

CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?i1 .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Negative Property Assertion" .
}
WHERE {
    ?x owl:sourceIndividual ?i1 .
    ?x owl:assertionProperty ?p .
    ?x owl:targetIndividual ?i2 .
    ?i1 ?p ?i2 .
}
```

### prp-npa2

``` {.query}
# prp-npa2
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?i .
    _:b0 spin:violationPath ?p .
    _:b0 rdfs:label "Negative Property Assertion" .
}
WHERE {
    ?x owl:sourceIndividual ?i .
    ?x owl:assertionProperty ?p .
    ?x owl:targetValue ?lt .
    ?i ?p ?lt .
}
```

### prp-pdw

``` {.query}
# prp-pdw
CONSTRUCT {
    _:b0 a spin:ConstraintViolation .
    _:b0 spin:violationRoot ?x .
    _:b0 spin:violationPath ?p1 .
    _:b0 rdfs:label "Property declared disjoint with" .
}
WHERE {
    ?p1 owl:propertyDisjointWith ?p2 .
    ?x ?p1 ?y .
    ?x ?p2 ?y .
}
```

Rules
-----

### cax-eqc1

``` {.query}
# cax-eqc1
CONSTRUCT {
    ?x a ?c2 .
}
WHERE {
    ?c1 owl:equivalentClass ?c2 .
    ?x a ?c1 .
}
```

### cax-eqc2

``` {.query}
# cax-eqc2
CONSTRUCT {
    ?x a ?c1 .
}
WHERE {
    ?c1 owl:equivalentClass ?c2 .
    ?x a ?c2 .
}
```

### cax-sco

``` {.query}
# cax-sco
CONSTRUCT {
    ?x a ?c2 .
}
WHERE {
    ?c1 rdfs:subClassOf ?c2 .
    ?x a ?c1 .
}
```

### cls-avf

``` {.query}
# cls-avf
CONSTRUCT {
    ?v a ?y .
}
WHERE {
    ?x owl:allValuesFrom ?y .
    ?x owl:onProperty ?p .
    ?u a ?x .
    ?u ?p ?v .
}
```

### cls-hv1

``` {.query}
# cls-hv1
CONSTRUCT {
    ?u ?p ?y .
}
WHERE {
    ?x owl:hasValue ?y .
    ?x owl:onProperty ?p .
    ?u a ?x .
}
```

### cls-hv2

``` {.query}
# cls-hv2
CONSTRUCT {
    ?u a ?x .
}
WHERE {
    ?x owl:hasValue ?y .
    ?x owl:onProperty ?p .
    ?u ?p ?y .
}
```

### cls-int1

``` {.query}
# cls-int1
CONSTRUCT {
    ?y a ?c .
}
WHERE {
    ?c owl:intersectionOf ?x .
    ?x list:member ?type .
    ?y a ?type .
    FILTER (!owlrl:listContainsExtraType(?y, ?x)) .
}
```

### cls-int2

``` {.query}
# cls-int2
CONSTRUCT {
    ?y a ?ci .
}
WHERE {
    ?c owl:intersectionOf ?x .
    ?x list:member ?ci .
    ?y a ?c .
}
```

### cls-maxc2

``` {.query}
# cls-maxc2
CONSTRUCT {
    ?y1 owl:sameAs ?y2 .
}
WHERE {
    ?x owl:maxCardinality "1"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?u a ?x .
    ?u ?p ?y1 .
    ?u ?p ?y2 .
}
```

### cls-maxqc3

``` {.query}
# cls-maxqc3
CONSTRUCT {
    ?y1 owl:sameAs ?y2 .
}
WHERE {
    ?x owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?x owl:onClass ?c .
    ?u a ?x .
    ?u ?p ?y1 .
    ?y1 a ?c .
    ?u ?p ?y1 .
    ?y2 a ?c .
}
```

### cls-maxqc4

``` {.query}
# cls-maxqc4
CONSTRUCT {
    ?y1 owl:sameAs ?y2 .
}
WHERE {
    ?x owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger .
    ?x owl:onProperty ?p .
    ?x owl:onClass owl:Thing .
    ?u a ?x .
    ?u ?p ?y1 .
    ?u ?p ?y2 .
}
```

### cls-oo

``` {.query}
# cls-oo
CONSTRUCT {
    ?yi a ?c .
}
WHERE {
    ?c owl:oneOf ?x .
    ?x list:member ?yi .
}
```

### cls-svf1

``` {.query}
# cls-svf1
CONSTRUCT {
    ?u a ?x .
}
WHERE {
    ?x owl:someValuesFrom ?y .
    ?x owl:onProperty ?p .
    ?u ?p ?v .
    ?v a ?y .
}
```

### cls-svf2

``` {.query}
# cls-svf2
CONSTRUCT {
    ?u a ?x .
}
WHERE {
    ?x owl:someValuesFrom owl:Thing .
    ?x owl:onProperty ?p .
    ?u ?p ?v .
}
```

### cls-uni

``` {.query}
# cls-uni
CONSTRUCT {
    ?y a ?c .
}
WHERE {
    ?c owl:unionOf ?x .
    ?x list:member ?ci .
    ?y a ?ci .
}
```

### eq-rep-o

``` {.query}
# eq-rep-o
CONSTRUCT {
    ?s ?p ?o2 .
}
WHERE {
    ?o owl:sameAs ?o2 .
    ?s ?p ?o .
}
```

### eq-rep-p

``` {.query}
# eq-rep-p
CONSTRUCT {
    ?s ?p2 ?o .
}
WHERE {
    ?p owl:sameAs ?p2 .
    ?s ?p ?o .
}
```

### eq-rep-s

``` {.query}
# eq-rep-s
CONSTRUCT {
    ?s2 ?p ?o .
}
WHERE {
    ?s owl:sameAs ?s2 .
    ?s ?p ?o .
}
```

### eq-sym

``` {.query}
# eq-sym
CONSTRUCT {
    ?y owl:sameAs ?x .
}
WHERE {
    ?x owl:sameAs ?y .
}
```

### eq-trans

``` {.query}
# eq-trans
CONSTRUCT {
    ?x owl:sameAs ?z .
}
WHERE {
    ?x owl:sameAs ?y .
    ?y owl:sameAs ?z .
}
```

### prp-dom

``` {.query}
# prp-dom
CONSTRUCT {
    ?x a ?c .
}
WHERE {
    ?p rdfs:domain ?c .
    ?x ?p ?y .
}
```

### prp-eqp1

``` {.query}
# prp-eqp1
CONSTRUCT {
    ?x ?p2 ?y .
}
WHERE {
    ?p1 owl:equivalentProperty ?p2 .
    ?x ?p1 ?y .
}
```

### prp-eqp2

``` {.query}
# prp-eqp2
CONSTRUCT {
    ?x ?p1 ?y .
}
WHERE {
    ?p1 owl:equivalentProperty ?p2 .
    ?x ?p2 ?y .
}
```

### prp-fp

``` {.query}
# prp-fp
CONSTRUCT {
    ?y1 owl:sameAs ?y2 .
}
WHERE {
    ?p a owl:FunctionalProperty .
    ?x ?p ?y1 .
    ?x ?p ?y2 .
    FILTER (?y1 != ?y2) .
}
```

### prp-ifp

``` {.query}
# prp-ifp
CONSTRUCT {
    ?x1 owl:sameAs ?x2 .
}
WHERE {
    ?p a owl:InverseFunctionalProperty .
    ?x1 ?p ?y .
    ?x2 ?p ?y .
    FILTER (?x1 != ?x2) .
}
```

### prp-inv1

``` {.query}
# prp-inv1
CONSTRUCT {
    ?y ?p2 ?x .
}
WHERE {
    ?p1 owl:inverseOf ?p2 .
    ?x ?p1 ?y .
}
```

### prp-inv2

``` {.query}
# prp-inv2
CONSTRUCT {
    ?y ?p1 ?x .
}
WHERE {
    ?p1 owl:inverseOf ?p2 .
    ?x ?p2 ?y .
}
```

### prp-key

``` {.query}
# prp-key
CONSTRUCT {
    ?x owl:sameAs ?y .
}
WHERE {
    ?c owl:hasKey ?u .
    ?x a ?c .
    ?y a ?c .
    FILTER (?x != ?y) .
    FILTER (!owlrl:keyViolationExists(?u, ?x, ?y)) .
}
```

### prp-rng

``` {.query}
# prp-rng
CONSTRUCT {
    ?y a ?c .
}
WHERE {
    ?p rdfs:range ?c .
    ?x ?p ?y .
}
```

### prp-spo1

``` {.query}
# prp-spo1
CONSTRUCT {
    ?x ?p2 ?y .
}
WHERE {
    ?p1 rdfs:subPropertyOf ?p2 .
    ?x ?p1 ?y .
}
```

### prp-spo2

``` {.query}
# prp-spo2
CONSTRUCT {
    ?subject ?p ?object .
}
WHERE {
    ?p owl:propertyChainAxiom ?x .
    ?x owlrl:propertyChainHelper ( ?subject ?object ) .
}
```

### prp-symp

``` {.query}
# prp-symp
CONSTRUCT {
    ?y ?p ?x .
}
WHERE {
    ?p a owl:SymmetricProperty .
    ?x ?p ?y .
}
```

### prp-trp

``` {.query}
# prp-trp
CONSTRUCT {
    ?x ?p ?z .
}
WHERE {
    ?p a owl:TransitiveProperty .
    ?x ?p ?y .
    ?y ?p ?z .
}
```

### scm-avf1

``` {.query}
# scm-avf1
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c2 .
}
WHERE {
    ?c1 owl:allValuesFrom ?y1 .
    ?c1 owl:onProperty ?p .
    ?c2 owl:allValuesFrom ?y2 .
    ?c2 owl:onProperty ?p .
    ?y1 rdfs:subClassOf ?y2 .
}
```

### scm-avf2

``` {.query}
# scm-avf2
CONSTRUCT {
    ?c2 rdfs:subClassOf ?c1 .
}
WHERE {
    ?c1 owl:allValuesFrom ?y .
    ?c1 owl:onProperty ?p1 .
    ?c2 owl:allValuesFrom ?y .
    ?c2 owl:onProperty ?p2 .
    ?p1 rdfs:subPropertyOf ?p2 .
}
```

### scm-cls

``` {.query}
# scm-cls
CONSTRUCT {
    ?c rdfs:subClassOf ?c .
    ?c owl:equivalentClass ?c .
    ?c rdfs:subClassOf owl:Thing .
    owl:Nothing rdfs:subClassOf ?c .
}
WHERE {
    ?c a owl:Class .
}
```

### scm-dom1

``` {.query}
# scm-dom1
CONSTRUCT {
    ?p rdfs:domain ?c2 .
}
WHERE {
    ?p rdfs:domain ?c1 .
    ?c1 rdfs:subClassOf ?c2 .
}
```

### scm-dom2

``` {.query}
# scm-dom2
CONSTRUCT {
    ?p1 rdfs:domain ?c .
}
WHERE {
    ?p2 rdfs:domain ?c .
    ?p1 rdfs:subPropertyOf ?p2 .
}
```

### scm-dp

``` {.query}
# scm-dp
CONSTRUCT {
    ?p rdfs:subPropertyOf ?p .
    ?p owl:equivalentProperty ?p .
}
WHERE {
    ?p a owl:DatatypeProperty .
}
```

### scm-eqc1

``` {.query}
# scm-eqc1
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c2 .
    ?c2 rdfs:subClassOf ?c1 .
}
WHERE {
    ?c1 owl:equivalentClass ?c2 .
}
```

### scm-eqc2

``` {.query}
# scm-eqc2
CONSTRUCT {
    ?c1 owl:equivalentClass ?c2 .
}
WHERE {
    ?c1 rdfs:subClassOf ?c2 .
    ?c2 rdfs:subClassOf ?c1 .
}
```

### scm-eqp1

``` {.query}
# scm-eqp1
CONSTRUCT {
    ?p1 rdfs:subPropertyOf ?p2 .
    ?p2 rdfs:subPropertyOf ?p1 .
}
WHERE {
    ?p1 owl:equivalentProperty ?p2 .
}
```

### scm-eqp2

``` {.query}
# scm-eqp2
CONSTRUCT {
    ?p1 owl:equivalentProperty ?p2 .
}
WHERE {
    ?p1 rdfs:subPropertyOf ?p2 .
    ?p2 rdfs:subPropertyOf ?p1 .
}
```

### scm-hv

``` {.query}
# scm-hv
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c2 .
}
WHERE {
    ?c1 owl:hasValue ?i .
    ?c1 owl:onProperty ?p1 .
    ?c2 owl:hasValue ?i .
    ?c2 owl:onProperty ?p2 .
    ?p1 rdfs:subPropertyOf ?p2 .
}
```

### scm-int

``` {.query}
# scm-int
CONSTRUCT {
    ?c rdfs:subClassOf ?cl .
}
WHERE {
    ?c owl:intersectionOf ?x .
    ?x list:member ?cl .
}
```

### scm-op

``` {.query}
# scm-op
CONSTRUCT {
    ?p rdfs:subPropertyOf ?p .
    ?p owl:equivalentProperty ?p .
}
WHERE {
    ?p a owl:ObjectProperty .
}
```

### scm-rng1

``` {.query}
# scm-rng1
CONSTRUCT {
    ?p rdfs:range ?c2 .
}
WHERE {
    ?p rdfs:range ?c1 .
    ?c1 rdfs:subClassOf ?c2 .
}
```

### scm-rng2

``` {.query}
# scm-rng2
CONSTRUCT {
    ?p1 rdfs:range ?c .
}
WHERE {
    ?p2 rdfs:range ?c .
    ?p1 rdfs:subPropertyOf ?p2 .
}
```

### scm-sco

``` {.query}
# scm-sco
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c3 .
}
WHERE {
    ?c1 rdfs:subClassOf ?c2 .
    ?c2 rdfs:subClassOf ?c3 .
}
```

### scm-spo

``` {.query}
# scm-spo
CONSTRUCT {
    ?p1 rdfs:subPropertyOf ?p3 .
}
WHERE {
    ?p1 rdfs:subPropertyOf ?p2 .
    ?p2 rdfs:subPropertyOf ?p3 .
}
```

### scm-svf1

``` {.query}
# scm-svf1
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c2 .
}
WHERE {
    ?c1 owl:someValuesFrom ?y1 .
    ?c1 owl:onProperty ?p .
    ?c2 owl:someValuesFrom ?y2 .
    ?c2 owl:onProperty ?p .
    ?y1 rdfs:subClassOf ?y2 .
}
```

### scm-svf2

``` {.query}
# scm-svf2
CONSTRUCT {
    ?c1 rdfs:subClassOf ?c2 .
}
WHERE {
    ?c1 owl:someValuesFrom ?y .
    ?c1 owl:onProperty ?p1 .
    ?c2 owl:someValuesFrom ?y .
    ?c2 owl:onProperty ?p2 .
    ?p1 rdfs:subPropertyOf ?p2 .
}
```

### scm-uni

``` {.query}
# scm-uni
CONSTRUCT {
    ?cl rdfs:subClassOf ?c .
}
WHERE {
    ?c owl:unionOf ?x .
    ?x list:member ?cl .
}
```
